package mx.cucea.udg.techfest.Objects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hauweri on 1/10/18.
 */

public class Story {
    String section;
    String subsection;
    String title;
    @SerializedName("abstract")
    String story_abstract;
    String url;
    String byline;
    String item_type;
    String updated_date;
    String created_date;
    String published_date;
    String material_type_facet;
    String kicker;
    String[] des_facet;
    String[] org_facet;
    String[] per_facet;
    String[] geo_facet;
    Multimedia[] multimedia;
    String short_url;

    public Story(String section, String subsection, String title,
                 String story_abstract, String url, String byline,
                 String item_type, String updated_date,
                 String created_date, String published_date,
                 String material_type_facet, String kicker,
                 String[] des_facet, String[] org_facet, String[] per_facet,
                 String[] geo_facet, Multimedia[] multimedia, String short_url) {
        this.section = section;
        this.subsection = subsection;
        this.title = title;
        this.story_abstract = story_abstract;
        this.url = url;
        this.byline = byline;
        this.item_type = item_type;
        this.updated_date = updated_date;
        this.created_date = created_date;
        this.published_date = published_date;
        this.material_type_facet = material_type_facet;
        this.kicker = kicker;
        this.des_facet = des_facet;
        this.org_facet = org_facet;
        this.per_facet = per_facet;
        this.geo_facet = geo_facet;
        this.multimedia = multimedia;
        this.short_url = short_url;
    }

    public String getSection() {
        return section;
    }

    public String getSubsection() {
        return subsection;
    }

    public String getTitle() {
        return title;
    }

    public String getStory_abstract() {
        return story_abstract;
    }

    public String getUrl() {
        return url;
    }

    public String getByline() {
        return byline;
    }

    public String getItem_type() {
        return item_type;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public String getCreated_date() {
        return created_date;
    }

    public String getPublished_date() {
        return published_date;
    }

    public String getMaterial_type_facet() {
        return material_type_facet;
    }

    public String getKicker() {
        return kicker;
    }

    public String[] getDes_facet() {
        return des_facet;
    }

    public String[] getOrg_facet() {
        return org_facet;
    }

    public String[] getPer_facet() {
        return per_facet;
    }

    public String[] getGeo_facet() {
        return geo_facet;
    }

    public Multimedia[] getMultimedia() {
        return multimedia;
    }

    public String getShort_url() {
        return short_url;
    }
}
