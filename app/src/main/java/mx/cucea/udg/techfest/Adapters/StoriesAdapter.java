package mx.cucea.udg.techfest.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.cucea.udg.techfest.Objects.Story;
import mx.cucea.udg.techfest.R;

/**
 * Created by hauweri on 1/10/18.
 */


public class StoriesAdapter extends RecyclerView.Adapter<StoriesAdapter.ViewHolder> {
    private List<Story> mStories;
    Context context;

    public StoriesAdapter(List<Story> stories, Context context){
        mStories = stories;
        this.context = context;
    }

    @Override
    public StoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View storyView = inflater.inflate(R.layout.story_item, parent,false);
        ViewHolder viewHolder = new ViewHolder(storyView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(StoriesAdapter.ViewHolder viewHolder, int position) {

    }

    @Override
    public int getItemCount() {
        return mStories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    String dateFormat(String dateLarge){

        String dateShort = "";
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("Y MMM d");

        try {
            Date d = input.parse(dateLarge);
            dateShort = output.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateShort;
    }
}
