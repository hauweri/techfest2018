package mx.cucea.udg.techfest.Objects;

/**
 * Created by hauweri on 1/10/18.
 */

public class Multimedia {
    String url;
    String format;
    String height;
    String width;
    String type;
    String subtype;
    String caption;
    String copyright;

    public Multimedia(String url, String format, String height,
                      String width, String type, String subtype,
                      String caption, String copyright) {
        this.url = url;
        this.format = format;
        this.height = height;

        this.width = width;
        this.type = type;
        this.subtype = subtype;
        this.caption = caption;
        this.copyright = copyright;
    }

    public String getUrl() {
        return url;
    }

    public String getFormat() {
        return format;
    }

    public String getHeight() {
        return height;
    }

    public String getWidth() {
        return width;
    }

    public String getType() {
        return type;
    }

    public String getSubtype() {
        return subtype;
    }

    public String getCaption() {
        return caption;
    }

    public String getCopyright() {
        return copyright;
    }
}
